'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _expressJwt = require('express-jwt');

var _expressJwt2 = _interopRequireDefault(_expressJwt);

var _config = require('./config/config');

var _config2 = _interopRequireDefault(_config);

var _MyPassport = require('./config/MyPassport');

var _MyPassport2 = _interopRequireDefault(_MyPassport);

var _LoginRoute = require('./routes/LoginRoute');

var _LoginRoute2 = _interopRequireDefault(_LoginRoute);

var _OrderRoute = require('./routes/OrderRoute');

var _OrderRoute2 = _interopRequireDefault(_OrderRoute);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var env = process.env.NODE_ENV || 'development';
//import passport from 'passport';

var config = _config2.default[env];


//create express application
var app = (0, _express2.default)();

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({
    extended: false
}));

// jwt check token
app.use((0, _expressJwt2.default)({
    secret: config.auth.secret,
    getToken: function getToken(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === "Bearer") {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({
    //Array where the exceptions must be inserted
    path: ["/api/login", '/', "/index.html"]
}));

var myPassport = new _MyPassport2.default();
myPassport.usePassport(app);

//route settings
app.use("/api", new _LoginRoute2.default().Router);
app.use("/api", new _OrderRoute2.default().Router);

//default get from root
app.get('/', function (req, res) {
    res.sendFile(_path2.default.join(__dirname + '/index.html'));
});

//start listening
var port = process.env.PORT || config.express.port;
app.listen(port, function () {
    console.log('API running on localhost:' + port); //eslint-disable-line no-console
});
//# sourceMappingURL=index.js.map