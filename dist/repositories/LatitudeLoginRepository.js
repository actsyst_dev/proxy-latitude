'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _LatitudeConnection = require('./LatitudeConnection');

var _LatitudeConnection2 = _interopRequireDefault(_LatitudeConnection);

var _LatitudeSubject = require('./LatitudeSubject');

var _LatitudeSubject2 = _interopRequireDefault(_LatitudeSubject);

var _Person = require('../models/Person');

var _Person2 = _interopRequireDefault(_Person);

var _Order = require('../models/Order');

var _Order2 = _interopRequireDefault(_Order);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class responsible for accessing data via Latitude regarding operators
*/
var LatitudeLoginRepository = function () {

    /**
     * Default constructor
     */
    function LatitudeLoginRepository() {
        _classCallCheck(this, LatitudeLoginRepository);

        this._latitudeSubject = new _LatitudeSubject2.default();
    }

    /**
     * Gets the LatitudeSubject acting as mediator
     * and allowing to subscribe to the publications of the LatitudeConnection
     */


    _createClass(LatitudeLoginRepository, [{
        key: 'getPerson',


        /**
         * Sends a request to get the person whose login and password are passed as parameters
         * @param {String} login Login of the person to get
         * @param {String} password Password of the person to get
         */
        value: function getPerson(login, password) {
            //TODO : call LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 801, login + "|" + password);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    if (results.length > 0) {
                        var person = new _Person2.default();
                        for (var i = 0; i < results.length; i++) {
                            //the result may contains many results if there are many orders
                            //linked to the person. Each result contains the data of the person
                            //and of the order.
                            //we instanciate only one person, so we check if it's the first
                            //round of the loop
                            if (i === 0) {
                                person.parse(results[i]);
                            }
                            //for each results, we instanciate an order
                            //and add it to the orders of the person
                            var order = new _Order2.default();
                            order.parse(results[i]);
                            person.addOrder(order);
                        }
                        //finally, we publish the person
                        that._latitudeSubject.publishLoginResult(person);
                    }
                } else {
                    //we publish the error
                    that._latitudeSubject.publishLoginResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }
    }, {
        key: 'LatitudeSubject',
        get: function get() {
            return this._latitudeSubject;
        }
    }]);

    return LatitudeLoginRepository;
}();

exports.default = LatitudeLoginRepository;
//# sourceMappingURL=LatitudeLoginRepository.js.map