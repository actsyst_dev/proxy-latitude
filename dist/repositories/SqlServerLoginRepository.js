"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class responsible for accessing data via SqlServer regarding operators
*/
var SqlServerLoginRepository = function () {
  function SqlServerLoginRepository() {
    _classCallCheck(this, SqlServerLoginRepository);
  }

  _createClass(SqlServerLoginRepository, [{
    key: "getPerson",


    /**
     * Sends a request to get the person whose login and password are passed as parameters
     * @param {String} login Login of the person to get
     * @param {String} password Password of the person to get
     */
    value: function getPerson(login, password) {
      console.log("Get person from SqlServer\r\nLogin : " + login + "\r\nPassword : " + password); //eslint-disable-line no-console
    }
  }]);

  return SqlServerLoginRepository;
}();

exports.default = SqlServerLoginRepository;
//# sourceMappingURL=SqlServerLoginRepository.js.map