'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.repoFact = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = require('../config/config');

var _config2 = _interopRequireDefault(_config);

var _SqlServerLoginRepository = require('./SqlServerLoginRepository');

var _SqlServerLoginRepository2 = _interopRequireDefault(_SqlServerLoginRepository);

var _LatitudeLoginRepository = require('./LatitudeLoginRepository');

var _LatitudeLoginRepository2 = _interopRequireDefault(_LatitudeLoginRepository);

var _LatitudeOrderRepository = require('./LatitudeOrderRepository');

var _LatitudeOrderRepository2 = _interopRequireDefault(_LatitudeOrderRepository);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var env = process.env.NODE_ENV || 'development';

var config = _config2.default[env];

/**
* @description Class responsible for creating repositories based on configuration and type
*/
var RepositoryFactory = function () {
    function RepositoryFactory() {
        _classCallCheck(this, RepositoryFactory);
    }

    _createClass(RepositoryFactory, [{
        key: 'makeRepository',


        /**
         * @description Makes and returns a repository based on configuration
         * @param {String} repotype Type of the repository to create
         * @returns A repository based on configuration and type
         */
        value: function makeRepository(repoType) {
            if (config.repository.latitude) {
                if (repoType === 'login') {
                    return new _LatitudeLoginRepository2.default();
                } else if (repoType === 'order') {
                    return new _LatitudeOrderRepository2.default();
                }
            } else if (config.repository.sqlServer) {
                if (repoType === 'login') {
                    return new _SqlServerLoginRepository2.default();
                }
            }
        }
    }]);

    return RepositoryFactory;
}();

//it allows to export a Singleton


var repoFact = exports.repoFact = new RepositoryFactory();
//# sourceMappingURL=RepositoryFactory.js.map