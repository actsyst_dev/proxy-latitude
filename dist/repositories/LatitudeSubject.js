'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Rx = require('rxjs/Rx');

var _Rx2 = _interopRequireDefault(_Rx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class acting as a mediator and allowing the pub/sub pattern
 * Specialized for managing the pub/sub communication with LatitudeConnection
 */
var LatitudeSubject = function () {

    /**
     * Default constructor
     */
    function LatitudeSubject() {
        _classCallCheck(this, LatitudeSubject);

        this._connectionStateSubject = new _Rx2.default.Subject();
        this._sendResultSubject = new _Rx2.default.Subject();
        this._loginResultSubject = new _Rx2.default.Subject();
        this._orderResultSubject = new _Rx2.default.Subject();
        this._supplierEntrySubject = new _Rx2.default.Subject();
        this._addSupplierEntrySubject = new _Rx2.default.Subject();
        this._deleteSupplierEntrySubject = new _Rx2.default.Subject();
        this._deleteAllSupplierEntrySubject = new _Rx2.default.Subject();
        this._deleteSeveralSupplierEntrySubject = new _Rx2.default.Subject();
    }

    /**
     * @description Gets a subject allowing to be notified about the connection state
     */


    _createClass(LatitudeSubject, [{
        key: 'publishConnectionState',


        /**
         * @description Publishes the state of the connection
         */
        value: function publishConnectionState() {
            this._connectionStateSubject.next({
                stateOn: true
            });
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the query
         */

    }, {
        key: 'publishSendResult',


        /**
         * @description Publishes the result of the query
         */
        value: function publishSendResult(result) {
            this._sendResultSubject.next(result);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the login process
         */

    }, {
        key: 'publishLoginResult',


        /**
         * Publishes the result of the login process
         */
        value: function publishLoginResult(result) {
            this._loginResultSubject.next(result);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the get order process
         */

    }, {
        key: 'publishOrderResult',


        /**
         * @description Publishes the result of the get order process
         */
        value: function publishOrderResult(orderItems) {
            this._orderResultSubject.next(orderItems);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the get supplier entry process
         */

    }, {
        key: 'publishSupplierEntryResult',


        /**
         * @description Publishes the result of the get supplier entries process
         */
        value: function publishSupplierEntryResult(supplierEntries) {
            this._supplierEntrySubject.next(supplierEntries);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the add supplier entries process
         */

    }, {
        key: 'publishAddSupplierEntryResult',


        /**
         * @description Publishes the result of the add supplier entry process
         */
        value: function publishAddSupplierEntryResult(supplierEntries) {
            this._addSupplierEntrySubject.next(supplierEntries);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the delete supplier entry process
         */

    }, {
        key: 'publishDeleteSupplierEntryResult',


        /**
         * @description Publishes the result of the delete supplier entry process
         */
        value: function publishDeleteSupplierEntryResult(result) {
            this._deleteSupplierEntrySubject.next(result);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the delete all supplier entries process
         */

    }, {
        key: 'publishDeleteAllSupplierEntryResult',


        /**
         * @description Publishes the result of the delete all supplier entries process
         */
        value: function publishDeleteAllSupplierEntryResult(result) {
            this._deleteAllSupplierEntrySubject.next(result);
        }

        /**
         * @description Gets a subject allowing to be notified about the result of the delete several supplier entries process
         */

    }, {
        key: 'publishDeleteSeveralSupplierEntryResult',


        /**
         * @description Publishes the result of the delete several supplier entries process
         */
        value: function publishDeleteSeveralSupplierEntryResult(result) {
            this._deleteSeveralSupplierEntrySubject.next(result);
        }
    }, {
        key: 'ConnectionStateSubject',
        get: function get() {
            return this._connectionStateSubject;
        }
    }, {
        key: 'SendResultSubject',
        get: function get() {
            return this._sendResultSubject;
        }
    }, {
        key: 'LoginResultSubject',
        get: function get() {
            return this._loginResultSubject;
        }
    }, {
        key: 'OrderResultSubject',
        get: function get() {
            return this._orderResultSubject;
        }
    }, {
        key: 'SupplierEntryResultSubject',
        get: function get() {
            return this._supplierEntrySubject;
        }
    }, {
        key: 'AddSupplierEntryResultSubject',
        get: function get() {
            return this._addSupplierEntrySubject;
        }
    }, {
        key: 'DeleteSupplierEntryResultSubject',
        get: function get() {
            return this._deleteSupplierEntrySubject;
        }
    }, {
        key: 'DeleteAllSupplierEntryResultSubject',
        get: function get() {
            return this._deleteAllSupplierEntrySubject;
        }
    }, {
        key: 'DeleteSeveralSupplierEntryResultSubject',
        get: function get() {
            return this._deleteSeveralSupplierEntrySubject;
        }
    }]);

    return LatitudeSubject;
}();

exports.default = LatitudeSubject;
//# sourceMappingURL=LatitudeSubject.js.map