'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _LatitudeConnection = require('./LatitudeConnection');

var _LatitudeConnection2 = _interopRequireDefault(_LatitudeConnection);

var _LatitudeSubject = require('./LatitudeSubject');

var _LatitudeSubject2 = _interopRequireDefault(_LatitudeSubject);

var _OrderItem = require('../models/OrderItem');

var _OrderItem2 = _interopRequireDefault(_OrderItem);

var _SupplierEntry = require('../models/SupplierEntry');

var _SupplierEntry2 = _interopRequireDefault(_SupplierEntry);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class responsible for accessing data via Latitude regarding Orders
*/
var LatitudeOrderRepository = function () {

    /**
     * Default constructor
     */
    function LatitudeOrderRepository() {
        _classCallCheck(this, LatitudeOrderRepository);

        this._latitudeSubject = new _LatitudeSubject2.default();
    }

    /**
     * Gets the LatitudeSubject acting as mediator
     * and allowing to subscribe to the publications of the LatitudeConnection
     */


    _createClass(LatitudeOrderRepository, [{
        key: 'getOrderItems',


        /**
         * @description Gets the list of the order item linked to the order
         * whose number is passed as parameter
         * @param {String} numberOrder Number of the order
         */
        value: function getOrderItems(numberOrder) {
            //TODO : get LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 802, numberOrder);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    var orderItems = [];
                    //for each results, we instanciate an orderItem
                    for (var i = 0; i < results.length; i++) {
                        var orderItem = new _OrderItem2.default();
                        var r = results[i];
                        orderItem.parse(r);
                        orderItems.push(orderItem);
                    }
                    //finally, we publish the list of the order items
                    that._latitudeSubject.publishOrderResult(orderItems);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishOrderResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }

        /**
         * @description Gets the list of the supplier entries linked to the order item
         * whose id is passed as parameter
         * @param {Number} itemId Id of the order item
         */

    }, {
        key: 'getSupplierEntries',
        value: function getSupplierEntries(itemId) {
            //TODO : get LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 803, itemId);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    var supplierEntries = [];
                    //for each results, we instanciate a SupplierEntry
                    for (var i = 0; i < results.length; i++) {
                        var supplierEntry = new _SupplierEntry2.default();
                        var r = results[i];
                        supplierEntry.parse(r);
                        supplierEntries.push(supplierEntry);
                    }
                    //we publish the list of the supplier entries
                    that._latitudeSubject.publishSupplierEntryResult(supplierEntries);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishSupplierEntryResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }

        /**
         * @description Adds a supplier entry
         * @param {SupplierEntry} supplierEntry The supplier entry to add
         */

    }, {
        key: 'addSupplierEntry',
        value: function addSupplierEntry(supplierEntry) {
            //TODO : Get LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 804, supplierEntry.ItemId + "|" + supplierEntry.LotNumber + "|" + supplierEntry.SerialNumber + "|" + supplierEntry.PeremptionDate + "|" + supplierEntry.Quantity);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    var supplierEntries = [];
                    //the query returns all the supplier entries linked to the same order item
                    //for each results, we instanciate a SupplierEntry
                    for (var i = 0; i < results.length; i++) {
                        var _supplierEntry = new _SupplierEntry2.default();
                        var r = results[i];
                        _supplierEntry.parse(r);
                        supplierEntries.push(_supplierEntry);
                    }
                    //we publish the list of the supplier entries
                    that._latitudeSubject.publishAddSupplierEntryResult(supplierEntries);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishAddSupplierEntryResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }

        /**
         * @description Delete the supplier entry whose the id is passed as parameter
         * @param {Number} supplierEntryId The id of the supplier entry to delete
         */

    }, {
        key: 'deleteSupplierEntry',
        value: function deleteSupplierEntry(supplierEntryId) {
            //TODO : Get the LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 805, supplierEntryId);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    var supplierEntries = [];
                    //the query returns all the supplier entries linked to the same order item
                    //for each results, we instanciate a SupplierEntry
                    for (var i = 0; i < results.length; i++) {
                        var supplierEntry = new _SupplierEntry2.default();
                        var r = results[i];
                        supplierEntry.parse(r);
                        supplierEntries.push(supplierEntry);
                    }
                    //we publish the list of the supplier entries
                    that._latitudeSubject.publishDeleteSupplierEntryResult(supplierEntries);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishDeleteSupplierEntryResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }

        /**
         * @description Delete all the supplier entries linked to the order item
         * whose the id is passed as parameter
         * @param {Number} itemId The id of the order item
         */

    }, {
        key: 'deleteAllSupplierEntries',
        value: function deleteAllSupplierEntries(itemId) {
            //TODO : Gets the LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we send the query
                    lc.send(1, 806, itemId);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //no Latitude error
                    var supplierEntries = [];
                    //the query returns all the supplier entries linked to the order item
                    //for each results, we instanciate a SupplierEntry
                    //normally, the list should be empty
                    for (var i = 0; i < results.length; i++) {
                        var supplierEntry = new _SupplierEntry2.default();
                        var r = results[i];
                        supplierEntry.parse(r);
                        supplierEntries.push(supplierEntry);
                    }
                    //we publish the list of the supplier entries
                    that._latitudeSubject.publishDeleteAllSupplierEntryResult(supplierEntries);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishDeleteAllSupplierEntryResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }

        /**
         * @description Delete the supplier entries whose the id is contained
         * in the array passed as parameter
         * @param {Array} supplierEntriesId Array containing the id of the supplier entries to delete
         */

    }, {
        key: 'deleteSeveralSupplierEntries',
        value: function deleteSeveralSupplierEntries(supplierEntriesId) {
            //TODO : Gets the LatitudeConnection from the pooler
            var that = this;
            //we subscribe to a connection state subject to be notified of the result
            //of the Latitude connexion attempt
            this._latitudeSubject.ConnectionStateSubject.subscribe(function (result) {
                if (result.stateOn) {
                    //Successful connection, we join the elements of the array and send the query
                    var parameters = supplierEntriesId.join();
                    lc.send(1, 807, parameters);
                } else {
                    //Connection or authentication failed
                    //TODO : management of the error
                    console.log('Error'); //eslint-disable-line no-console
                }
            });

            //we subscribe to a send result subject to be notified of the query result
            this._latitudeSubject.SendResultSubject.subscribe(function (results) {
                if (results.indexOf('Latitude error') === -1) {
                    //No Latitude error
                    var supplierEntries = [];
                    //the query returns all the supplier entries linked to the same order item
                    //for each results, we instanciate a SupplierEntry
                    for (var i = 0; i < results.length; i++) {
                        var supplierEntry = new _SupplierEntry2.default();
                        var r = results[i];
                        supplierEntry.parse(r);
                        supplierEntries.push(supplierEntry);
                    }
                    //we publish the list of the supplier entries
                    that._latitudeSubject.publishDeleteSeveralSupplierEntryResult(supplierEntries);
                } else {
                    //we publish the error
                    that._latitudeSubject.publishDeleteSeveralSupplierEntryResult(results);
                }
            });
            //we instanciate a LatitudeConnection
            //and pass it the LatitudeSubject wich allows the pub/sub communication
            var lc = new _LatitudeConnection2.default(this._latitudeSubject);
            //we ask the socket connection and the sending of the Latitude authentication frame
            lc.initializeConnection();
        }
    }, {
        key: 'LatitudeSubject',
        get: function get() {
            return this._latitudeSubject;
        }
    }]);

    return LatitudeOrderRepository;
}();

exports.default = LatitudeOrderRepository;
//# sourceMappingURL=LatitudeOrderRepository.js.map