'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _net = require('net');

var _net2 = _interopRequireDefault(_net);

var _windows = require('windows-1252');

var _windows2 = _interopRequireDefault(_windows);

var _config = require('../config/config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var env = process.env.NODE_ENV || 'development';

var config = _config2.default[env];

/**
 * @description Class responsible for communicating with Latitude. Implements the Latitude protocol.
 */

var LatitudeConnection = function () {

    /**
     * @description Constructor
     * @param {LatitudeSubject} latitudeSubject LatitudeSuject acting as mediator and allowing
     * the publication of the query results
     */
    function LatitudeConnection(latitudeSubject) {
        _classCallCheck(this, LatitudeConnection);

        this._socket = null;
        this._pendingDatas = '';
        this._latitudeSubject = latitudeSubject;
    }

    /**
     * Instanciates a socket to Latitude and sends the authentication frame
     * Implements also the receiver function of the socket object
     */


    _createClass(LatitudeConnection, [{
        key: 'initializeConnection',
        value: function initializeConnection() {
            var _this = this;

            //instanciates a socket and sends the latitude authentication frame
            this._socket = _net2.default.connect(config.latitude.port, config.latitude.host, function () {
                //TODO : automating the determination of the latitude credentials
                var authString = String.fromCharCode(2) + 'PW###001|LAN1|LAN1' + String.fromCharCode(3);
                var encodedData = _windows2.default.encode(authString);
                _this._socket.write(encodedData);
            });
            var that = this;
            //receiving socket callback
            this._socket.on("data", function (data) {
                if (data[data.length - 1] === 3) {
                    //end of the frame, we delete the ETX character
                    for (var i = 0; i < data.length - 1; i++) {
                        that._pendingDatas = that._pendingDatas + String.fromCharCode(data[i]);
                    }
                } else {
                    //we add to receiving to the pending data
                    for (var _i = 0; _i < data.length; _i++) {
                        that._pendingDatas = that._pendingDatas + String.fromCharCode(data[_i]);
                    }
                }
                //we check if it's the end of the transmitting
                if (that._pendingDatas.endsWith("#008#") || that._pendingDatas.endsWith("OK") || that._pendingDatas.endsWith("NOK")) {
                    var datas = that.getArrayFromFrame();
                    that.onComplete(datas);
                }
                //we check if the query result is empty
                else if (that._pendingDatas.endsWith("#005#")) {
                        that.onComplete("");
                    }
                    //we check if the query caused an error
                    else if (that._pendingDatas.endsWith("#099#") || that._pendingDatas.endsWith("#006#") || that._pendingDatas.endsWith("#550#") || that._pendingDatas.endsWith("#849#") || that._pendingDatas.endsWith("#756#") || that._pendingDatas.endsWith("#1300#")) {
                            that.onComplete("Latitude error");
                        }
            });
            //error socket callback
            this._socket.on("error", function (err) {
                console.log("Erreur de connexion!" + err.message); //eslint-disable-line no-console
                //TODO try reconnection
            });
        }

        /**
         * Sends a query to Latitude
         * @param {Number} databaseNumber Database number to query
         * @param {Number} queryNumber Query number
         * @param {String} datas Criteria separated by the character |
         */

    }, {
        key: 'send',
        value: function send(databaseNumber, queryNumber, datas) {
            var queryString = String.fromCharCode(2) + 'SE#MID#' + databaseNumber + '#' + queryNumber + '#' + datas + String.fromCharCode(3);
            var encodedData = _windows2.default.encode(queryString);
            this._pendingDatas = '';
            this._socket.write(encodedData);
        }

        /**
         * Gets an array containing the result of the query
         */

    }, {
        key: 'getArrayFromFrame',
        value: function getArrayFromFrame() {
            var retval = [];
            if (this._pendingDatas.endsWith("NOK")) {
                //authentication failed
                retval.push("NOK");
            } else if (this._pendingDatas.endsWith("OK")) {
                //Successful authentication
                retval.push("OK");
            } else {
                //records case
                while (this._pendingDatas.indexOf("SE#MID#000#") > -1) {
                    this._pendingDatas = this._pendingDatas.replace("SE#MID#000#", "");
                }
                retval = this._pendingDatas.split(String.fromCharCode(2));
            }
            return retval;
        }

        /**
         * Callback receiving the result of the query. Publish it with the LatitudeSubject
         * @param {Array} data Result of the query
         */

    }, {
        key: 'onComplete',
        value: function onComplete(data) {
            //publish to subscripters
            if (data[0] === 'OK') {
                //Successful authentication
                this._latitudeSubject.publishConnectionState({
                    stateOn: true
                });
            } else if (data[0] === 'NOK') {
                //authentication failed
                this._latitudeSubject.publishConnectionState({
                    stateOn: false
                });
            } else if (data === 'Latitude error') {
                //the query caused an error
                this._latitudeSubject.publishSendResult('Latitude error');
            } else {
                //records case
                var objects = [];
                for (var i = 0; i < data.length - 1; i++) {
                    if (data[i] !== "" && !data[i].endsWith("SE#MID#008#")) {
                        objects.push(data[i].replace(String.fromCharCode(3), ''));
                    }
                }
                //the query caused a validation error
                if (objects.length > 0 && objects[0].indexOf("$VALIDATION_ERROR$") !== -1) {
                    this._latitudeSubject.publishSendResult('Latitude error : ' + objects[0]);
                }
                //the query returned records
                else {
                        this._latitudeSubject.publishSendResult(objects);
                    }
            }
        }
    }]);

    return LatitudeConnection;
}();

exports.default = LatitudeConnection;
//# sourceMappingURL=LatitudeConnection.js.map