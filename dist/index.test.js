'use strict';

var _chai = require('chai');

var _jsdom = require('jsdom');

var _jsdom2 = _interopRequireDefault(_jsdom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Our first test', function () {
    it('should pass', function () {
        (0, _chai.expect)(true).to.equal(true);
    });
});

describe('index.html', function () {
    it('should have h2 that says hello proxy lattitude', function () {
        var JSDOM = _jsdom2.default.JSDOM;

        return new Promise(function (resolve, reject) {
            JSDOM.fromFile("./src/index.html", null).then(function (dom) {
                var h2 = dom.window.document.getElementsByTagName('h2')[0];
                (0, _chai.expect)(h2.innerHTML).to.equal("hello proxy lattitude");
                dom.window.close();
                return resolve();
            }).catch(function (error) {
                return reject(error);
            });
        });
    });
});
//# sourceMappingURL=index.test.js.map