"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _passport = require("passport");

var _passport2 = _interopRequireDefault(_passport);

var _LocalStrategy = require("./strategies/LocalStrategy");

var _LocalStrategy2 = _interopRequireDefault(_LocalStrategy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class responsible for initializing and setting the passport with a local strategy
 */
var MyPassport = function () {
  function MyPassport() {
    _classCallCheck(this, MyPassport);
  }

  _createClass(MyPassport, [{
    key: "usePassport",


    /**
     * @description Set the passport with a local strategy, initialize it,
     * and mount it to the express application passed in parameter
     * @param {Express} app Express application that will use the passport
     */
    value: function usePassport(app) {
      //we instanciate a LocalStrategy object and apply the builtin strategy
      var ls = new _LocalStrategy2.default();
      ls.applyStrategy();
      //we mount the passport object initialized to the express application
      app.use(_passport2.default.initialize());
    }
  }]);

  return MyPassport;
}();

exports.default = MyPassport;
//# sourceMappingURL=MyPassport.js.map