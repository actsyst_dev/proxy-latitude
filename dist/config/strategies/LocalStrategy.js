'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _RepositoryFactory = require('../../repositories/RepositoryFactory');

var _passportLocal = require('passport-local');

var _passportLocal2 = _interopRequireDefault(_passportLocal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Class responsible for applying a passport local strategy for authenticating
 * with login and password
*/
var LocalStrategy = function () {
    function LocalStrategy() {
        _classCallCheck(this, LocalStrategy);
    }

    _createClass(LocalStrategy, [{
        key: 'applyStrategy',


        /**
         * @description Apply the passport local strategy
         */
        value: function applyStrategy() {
            var LocStra = _passportLocal2.default.Strategy;

            //set the passport to use a local strategy
            _passport2.default.use(new LocStra({
                //we'll use login and password fields
                usernameField: 'login',
                passwordField: 'password'
            }, function (login, password, done) {
                //ask to the factory witch repository to use
                var repo = _RepositoryFactory.repoFact.makeRepository('login');
                //the repository provides a pub/sub mecanism to be notified of the response
                //a LatitudeSubject object managing this pub/sub mecanism is exposed by the repository
                //we use the LatitudeSubject by subscribing to the LoginResultSubject that it provides
                repo.LatitudeSubject.LoginResultSubject.subscribe(function (person) {
                    //we get the response
                    return done(null, person);
                });
                //we ask to the repository to get the person via is own data access mecanism
                repo.getPerson(login, password);
            }));
        }
    }]);

    return LocalStrategy;
}();

exports.default = LocalStrategy;
//# sourceMappingURL=LocalStrategy.js.map