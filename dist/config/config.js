'use strict';

module.exports = {
    development: {
        auth: {
            secret: 'abcdefActyst'
        },
        express: {
            port: '3005'
        },
        repository: {
            latitude: true,
            sqlServer: false
        },
        latitude: {
            host: '217.128.121.44',
            port: '8530'
        }
    },
    production: {
        auth: {
            secret: 'abcdefActyst'
        },
        express: {
            port: '3005'
        },
        repository: {
            latitude: true,
            sqlServer: false
        },
        latitude: {
            host: '217.128.121.44',
            port: '8530'
        }
    }
};
//# sourceMappingURL=config.js.map