'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('../config/config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var env = process.env.NODE_ENV || 'development';

var config = _config2.default[env];

/**
 * @description Business class implementing an person
 */

var Person = function () {

    /**
     * Default constructor
     */
    function Person() {
        _classCallCheck(this, Person);

        this.id = -1;
        this.login = '';
        this.password = '';
        this.firstName = '';
        this.lastName = '';
        this.qualification = '';
        this.token = '';
        this._orders = [];
    }

    /**
     * Generates a security token by using jwt library and inner properties
     */


    _createClass(Person, [{
        key: 'generateToken',
        value: function generateToken() {
            var expiry = new Date();
            expiry.setDate(expiry.getDate() + 7);
            this.token = _jsonwebtoken2.default.sign({
                email: this.login,
                exp: parseInt(expiry.getTime() / 1000)
            }, config.auth.secret);
            return this.token;
        }

        /**
         * @description Splits a string by applying the Latitude protocol
         *  and sets the properties of the object
         * @param {String} data Data string from Latitude to split
         */

    }, {
        key: 'parse',
        value: function parse(data) {
            var datas = data.split('|');
            this.id = new Number(datas[0]);
            this.firstName = datas[1];
            this.lastName = datas[2];
            this.login = datas[4];
            this.password = datas[5];
        }

        /**
         * Adds an order to the orders array of the person
         * @param {Order} order Order to add
         */

    }, {
        key: 'addOrder',
        value: function addOrder(order) {
            this._orders.push(order);
        }
    }]);

    return Person;
}();

exports.default = Person;
//# sourceMappingURL=Person.js.map