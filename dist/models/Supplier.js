'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Business class implementing an supplier
 */
var Supplier = function () {

    /**
     * Default constructor
     */
    function Supplier() {
        _classCallCheck(this, Supplier);

        this._code = '';
        this._name = '';
        this._country = '';
    }

    /**
     * @description Sets the code of the supplier
     * @param {String} code Code to set
     */


    _createClass(Supplier, [{
        key: 'Code',
        set: function set(code) {
            this._code = code;
        }

        /**
         * @description Sets the name of the supplier
         * @param {String} name Name to set
         */

    }, {
        key: 'Name',
        set: function set(name) {
            this._name = name;
        }

        /**
         * @description Sets the country of the supplier
         * @param {String} country Country to set
         */

    }, {
        key: 'Country',
        set: function set(country) {
            this._country = country;
        }
    }]);

    return Supplier;
}();

exports.default = Supplier;
//# sourceMappingURL=Supplier.js.map