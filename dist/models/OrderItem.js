'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Article = require('./Article');

var _Article2 = _interopRequireDefault(_Article);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Business class implementing an order item
 */
var OrderItem = function () {

    /**
     * Default constructor
     */
    function OrderItem() {
        _classCallCheck(this, OrderItem);

        this._itemId = new Number(0);
        this._article = new _Article2.default();
        this._orderedQuantity = new Number(0);
        this._printedQuantity = new Number(0);
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */


    _createClass(OrderItem, [{
        key: 'parse',
        value: function parse(data) {
            var datas = data.split('|');
            this._itemId = new Number(datas[0]);
            this._article.Ref = datas[1];
            this._article.Designation = datas[2];
            this._article.IsLotNumber = new Boolean(datas[3] === '1');
            this._article.IsSerialNumber = new Boolean(datas[4] === '1');
            this._article.IsPeremptionDate = new Boolean(datas[5] === '1');
            this._orderedQuantity = new Number(datas[6]);
            this._printedQuantity = new Number(datas[7]);
            this._article.SubRef1 = datas[8];
            this._article.SubRef2 = datas[9];
        }
    }]);

    return OrderItem;
}();

exports.default = OrderItem;
//# sourceMappingURL=OrderItem.js.map