'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Business class implementing an supplier entry
 */
var SupplierEntry = function () {

    /**
     * Default constructor
     */
    function SupplierEntry() {
        _classCallCheck(this, SupplierEntry);

        this._supplierEntryId = new Number(0);
        this._itemId = new Number(0);
        this._lotNumber = '';
        this._serialNumber = '';
        this._peremptionDate = '';
        this._quantity = new Number(0);
    }

    /**
     * @description Gets the id of the supplier entry
     */


    _createClass(SupplierEntry, [{
        key: 'parse',


        /**
         * @description Splits a string by applying the Latitude protocol
         *  and sets the properties of the object
         * @param {String} data Data string from Latitude to split
         */
        value: function parse(data) {
            var datas = data.split('|');
            this._supplierEntryId = new Number(datas[0]);
            this._lotNumber = datas[1];
            this._serialNumber = datas[2];
            this._peremptionDate = datas[3];
            this._quantity = new Number(datas[4]);
            this._itemId = new Number(datas[5]);
        }
    }, {
        key: 'SupplierEntryId',
        get: function get() {
            return this._supplierEntryId;
        }

        /**
         * @description Sets the id of the supplier entry
         * @param {Number} supplierEntryId Id to set
         */
        ,
        set: function set(supplierEntryId) {
            this._supplierEntryId = supplierEntryId;
        }

        /**
         * @description Gets the id of the order item linked to this supplier entry
         */

    }, {
        key: 'ItemId',
        get: function get() {
            return this._itemId;
        }

        /**
         * @description Sets the id of the order item linked to this supplier entry
         * @param {Number} itemId Id of the order item to set
         */
        ,
        set: function set(itemId) {
            this._itemId = itemId;
        }

        /**
         * @description Gets the lot number of the supplier entry
         */

    }, {
        key: 'LotNumber',
        get: function get() {
            return this._lotNumber;
        }

        /**
         * @description Sets the lot number of the supplier entry
         * @param {String} lotNumber Lot number of the supplier entry
         */
        ,
        set: function set(lotNumber) {
            this._lotNumber = lotNumber;
        }

        /**
         * @description Gets the serial number of the supplier entry
         */

    }, {
        key: 'SerialNumber',
        get: function get() {
            return this._serialNumber;
        }

        /**
         * @description Sets the serial number of the supplier entry
         * @param {String} serialNumber Serial number of the supplier entry
         */
        ,
        set: function set(serialNumber) {
            this._serialNumber = serialNumber;
        }

        /**
         * @description Gets the peremption date of the supplier entry
         */

    }, {
        key: 'PeremptionDate',
        get: function get() {
            return this._peremptionDate;
        }

        /**
         * @description Sets the peremption date of the supplier entry
         * @param {String} peremptionDate Peremption date of the supplier entry
         */
        ,
        set: function set(peremptionDate) {
            this._peremptionDate = peremptionDate;
        }

        /**
         * @description Gets the quantity of the supplier entry
         */

    }, {
        key: 'Quantity',
        get: function get() {
            return this._quantity;
        }

        /**
         * @description Sets the quantity of the supplier entry
         * @param {Number} quantity Quantity of the supplier entry
         */
        ,
        set: function set(quantity) {
            this._quantity = quantity;
        }
    }]);

    return SupplierEntry;
}();

exports.default = SupplierEntry;
//# sourceMappingURL=SupplierEntry.js.map