'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Business class implementing an article
 */
var Article = function () {

    /**
     * @description Default constructor
     */
    function Article() {
        _classCallCheck(this, Article);

        this._ref = '';
        this._subRef1 = '';
        this._subRef2 = '';
        this._designation = '';
        this._isLotNumber = false;
        this._isSerialNumber = false;
        this._isPeremptionDate = false;
    }

    /**
     * @description Gets the reference of the article
     */


    _createClass(Article, [{
        key: 'Ref',
        get: function get() {
            return this._ref;
        }

        /**
         * @description Sets the reference of the article
         * @param {String} ref Reference to set
         */
        ,
        set: function set(ref) {
            this._ref = ref;
        }

        /**
         * @description Gets the first sub reference of the article
         */

    }, {
        key: 'SubRef1',
        get: function get() {
            return this._subRef1;
        }

        /**
         * @description Sets the first sub reference of the article
         * @param {String} subRef1 First sub reference to set
         */
        ,
        set: function set(subRef1) {
            this._subRef1 = subRef1;
        }

        /**
         * @description Gets the second sub reference of the article
         */

    }, {
        key: 'SubRef2',
        get: function get() {
            return this._subRef2;
        }

        /**
         * @description Sets the second sub reference of the article
         * @param {String} subRef2 Second sub reference to set
         */
        ,
        set: function set(subRef2) {
            this._subRef2 = subRef2;
        }

        /**
         * @description Gets the designation of the article
         */

    }, {
        key: 'Designation',
        get: function get() {
            return this._designation;
        }

        /**
         * @description Sets the designation of the article
         * @param {String} designation Designation to set
         */
        ,
        set: function set(designation) {
            this._designation = designation;
        }

        /**
         * @description Gets a boolean that defines whether the article requires a lot number
         */

    }, {
        key: 'IsLotNumber',
        get: function get() {
            return this._isLotNumber;
        }

        /**
         * @description Sets a boolean that defines whether the article requires a lot number
         * @param {Boolean} isLotNumber True whether the article requires a lot number
         */
        ,
        set: function set(isLotNumber) {
            this._isLotNumber = isLotNumber;
        }

        /**
         * @description Gets a boolean that defines whether the article requires a serial number
         */

    }, {
        key: 'IsSerialNumber',
        get: function get() {
            return this._isSerialNumber;
        }

        /**
         * @description Sets a boolean that defines whether the article requires a serial number
         * @param {Boolean} isSerialNumber True whether the article requires a serial number
         */
        ,
        set: function set(isSerialNumber) {
            this._isSerialNumber = isSerialNumber;
        }

        /**
         * @description Gets a boolean that defines whether the article requires a peremption date
         */

    }, {
        key: 'IsPeremptionDate',
        get: function get() {
            return this._isPeremptionDate;
        }

        /**
         * @description Sets a boolean that defines whether the article requires a peremption date
         * @param {Boolean} isPeremptionDate True whether the article requires a peremption date
         */
        ,
        set: function set(isPeremptionDate) {
            this._isPeremptionDate = isPeremptionDate;
        }
    }]);

    return Article;
}();

exports.default = Article;
//# sourceMappingURL=Article.js.map