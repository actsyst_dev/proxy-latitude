'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Supplier = require('./Supplier');

var _Supplier2 = _interopRequireDefault(_Supplier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * @description Business class implementing an order
 */
var Order = function () {

    /**
     * Default constructor
     */
    function Order() {
        _classCallCheck(this, Order);

        this._orderItems = [];
        this._supplier;
        this._orderNumber;
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */


    _createClass(Order, [{
        key: 'parse',
        value: function parse(data) {
            var datas = data.split('|');
            this._orderNumber = datas[6];
            this._supplier = new _Supplier2.default();
            this._supplier.Code = datas[7];
            this._supplier.Name = datas[8];
        }
    }]);

    return Order;
}();

exports.default = Order;
//# sourceMappingURL=Order.js.map