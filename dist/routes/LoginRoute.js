'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LoginRoute = function () {
    function LoginRoute() {
        _classCallCheck(this, LoginRoute);

        this._router = _express2.default.Router();
        this.initialize();
    }

    _createClass(LoginRoute, [{
        key: 'initialize',
        value: function initialize() {
            this._router.post('/login', function (req, res, next) {
                //eslint-disable-line no-unused-vars
                //console.log(req.params);
                //console.log(req.body);

                _passport2.default.authenticate("local", function (err, person, info) {
                    if (err) {
                        res.status(404).json(err);
                        return;
                    }
                    if (person) {
                        //credentials checking
                        var token = person.generateToken();
                        res.status(200).json({
                            "token": token,
                            "person": person
                        });
                    } else {
                        res.status(401).json(info);
                    }
                })(req, res);
            });
        }
    }, {
        key: 'Router',
        get: function get() {
            return this._router;
        }
    }]);

    return LoginRoute;
}();

exports.default = LoginRoute;
//# sourceMappingURL=LoginRoute.js.map