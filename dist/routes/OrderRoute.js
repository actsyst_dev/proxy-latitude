'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _RepositoryFactory = require('../repositories/RepositoryFactory');

var _SupplierEntry = require('../models/SupplierEntry');

var _SupplierEntry2 = _interopRequireDefault(_SupplierEntry);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var OrderRoute = function () {
    function OrderRoute() {
        _classCallCheck(this, OrderRoute);

        this._router = _express2.default.Router();
        this.initialize();
    }

    _createClass(OrderRoute, [{
        key: 'initialize',
        value: function initialize() {
            this._router.get('/orderItems/:number', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var number = req.params.number;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.OrderResultSubject.subscribe(function (orderItems) {
                    if (typeof orderItems === "string" && orderItems.indexOf('Latitude error') !== -1) {
                        res.status(500).json(orderItems);
                    } else {
                        res.status(200).json(orderItems);
                    }
                });

                repo.getOrderItems(number);
            });

            this._router.get('/supplierEntries/:itemId', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var itemId = req.params.itemId;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.SupplierEntryResultSubject.subscribe(function (supplierEntry) {
                    if (typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error') !== -1) {
                        res.status(500).json(supplierEntry);
                    } else {
                        res.status(200).json(supplierEntry);
                    }
                });

                repo.getSupplierEntries(itemId);
            });

            this._router.post('/supplierEntries/add', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var supplierEntry = new _SupplierEntry2.default();
                supplierEntry.ItemId = req.body.itemId;
                supplierEntry.LotNumber = req.body.lotNumber;
                supplierEntry.SerialNumber = req.body.serialNumber;
                supplierEntry.PeremptionDate = req.body.peremptionDate;
                supplierEntry.Quantity = req.body.quantity;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.AddSupplierEntryResultSubject.subscribe(function (supplierEntry) {
                    if (typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error') !== -1) {
                        res.status(500).json(supplierEntry);
                    } else {
                        res.status(200).json(supplierEntry);
                    }
                });

                repo.addSupplierEntry(supplierEntry);
            });

            this._router.post('/supplierEntries/delete/:supplierEntryId', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var supplierEntryId = req.params.supplierEntryId;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.DeleteSupplierEntryResultSubject.subscribe(function (supplierEntry) {
                    if (typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error') !== -1) {
                        res.status(500).json(supplierEntry);
                    } else {
                        res.status(200).json(supplierEntry);
                    }
                });

                repo.deleteSupplierEntry(supplierEntryId);
            });

            this._router.post('/supplierEntries/deleteSeveral', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var supplierEntriesId = req.body;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.DeleteSeveralSupplierEntryResultSubject.subscribe(function (supplierEntry) {
                    if (typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error') !== -1) {
                        res.status(500).json(supplierEntry);
                    } else {
                        res.status(200).json(supplierEntry);
                    }
                });

                repo.deleteSeveralSupplierEntries(supplierEntriesId);
            });

            this._router.post('/supplierEntries/deleteAll/:itemId', function (req, res, next) {
                //eslint-disable-line no-unused-vars

                var itemId = req.params.itemId;

                var repo = _RepositoryFactory.repoFact.makeRepository('order');
                repo.LatitudeSubject.DeleteAllSupplierEntryResultSubject.subscribe(function (supplierEntry) {
                    if (typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error') !== -1) {
                        res.status(500).json(supplierEntry);
                    } else {
                        res.status(200).json(supplierEntry);
                    }
                });

                repo.deleteAllSupplierEntries(itemId);
            });
        }
    }, {
        key: 'Router',
        get: function get() {
            return this._router;
        }
    }]);

    return OrderRoute;
}();

exports.default = OrderRoute;
//# sourceMappingURL=OrderRoute.js.map