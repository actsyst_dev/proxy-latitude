import path from 'path';
import webpack from 'webpack';

export default {
  mode : 'development',
  devServer: {
    contentBase: path.resolve(__dirname, 'src'),
    noInfo: false
  },
  devtool: 'inline-source-map',
  entry: [
    path.resolve(__dirname, 'src/testwebpack')
  ],
  target: 'web',
  output: {
    path: path.resolve(__dirname, 'src'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ],
  module: {
    rules: [
        {
            test: /\.css$/,
            use: [ 'style-loader', 'css-loader' ]
        },
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['env']
              }
            }
        }
    ]
  }
}
