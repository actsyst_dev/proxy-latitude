import LatitudeConnection from './LatitudeConnection';
import LatitudeSubject from './LatitudeSubject';
import Person from '../models/Person';
import Order from '../models/Order';

/**
 * @description Class responsible for accessing data via Latitude regarding operators
*/
export default class LatitudeLoginRepository {

    /**
     * Default constructor
     */
    constructor() {
        this._latitudeSubject = new LatitudeSubject();
    }

    /**
     * Gets the LatitudeSubject acting as mediator
     * and allowing to subscribe to the publications of the LatitudeConnection
     */
    get LatitudeSubject() {
        return this._latitudeSubject;
    }

    /**
     * Sends a request to get the person whose login and password are passed as parameters
     * @param {String} login Login of the person to get
     * @param {String} password Password of the person to get
     */
    getPerson(login, password) {
        //TODO : call LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 801, login + "|" + password);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                if (results.length > 0) {
                    let person = new Person();
                    for (let i = 0; i < results.length; i++) {
                        //the result may contains many results if there are many orders
                        //linked to the person. Each result contains the data of the person
                        //and of the order.
                        //we instanciate only one person, so we check if it's the first
                        //round of the loop
                        if (i === 0) {
                            person.parse(results[i]);
                        }
                        //for each results, we instanciate an order
                        //and add it to the orders of the person
                        let order = new Order();
                        order.parse(results[i]);
                        person.addOrder(order);
                    }
                    //finally, we publish the person
                    that._latitudeSubject.publishLoginResult(person);
                }
            }
            else {
                //we publish the error
                that._latitudeSubject.publishLoginResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }
}
