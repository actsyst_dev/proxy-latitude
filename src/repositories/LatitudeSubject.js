import Rx from 'rxjs/Rx';

/**
 * @description Class acting as a mediator and allowing the pub/sub pattern
 * Specialized for managing the pub/sub communication with LatitudeConnection
 */
export default class LatitudeSubject {

    /**
     * Default constructor
     */
    constructor() {
        this._connectionStateSubject = new Rx.Subject();
        this._sendResultSubject = new Rx.Subject();
        this._loginResultSubject = new Rx.Subject();
        this._orderResultSubject = new Rx.Subject();
        this._supplierEntrySubject = new Rx.Subject();
        this._addSupplierEntrySubject = new Rx.Subject();
        this._deleteSupplierEntrySubject = new Rx.Subject();
        this._deleteAllSupplierEntrySubject = new Rx.Subject();
        this._deleteSeveralSupplierEntrySubject = new Rx.Subject();
    }

    /**
     * @description Gets a subject allowing to be notified about the connection state
     */
    get ConnectionStateSubject() {
        return this._connectionStateSubject;
    }

    /**
     * @description Publishes the state of the connection
     */
    publishConnectionState() {
        this._connectionStateSubject.next({
            stateOn: true
        });
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the query
     */
    get SendResultSubject() {
        return this._sendResultSubject;
    }

    /**
     * @description Publishes the result of the query
     */
    publishSendResult(result) {
        this._sendResultSubject.next(result);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the login process
     */
    get LoginResultSubject() {
        return this._loginResultSubject;
    }

    /**
     * Publishes the result of the login process
     */
    publishLoginResult(result) {
        this._loginResultSubject.next(result);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the get order process
     */
    get OrderResultSubject() {
        return this._orderResultSubject;
    }

    /**
     * @description Publishes the result of the get order process
     */
    publishOrderResult(orderItems) {
        this._orderResultSubject.next(orderItems);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the get supplier entry process
     */
    get SupplierEntryResultSubject() {
        return this._supplierEntrySubject;
    }

    /**
     * @description Publishes the result of the get supplier entries process
     */
    publishSupplierEntryResult(supplierEntries) {
        this._supplierEntrySubject.next(supplierEntries);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the add supplier entries process
     */
    get AddSupplierEntryResultSubject() {
        return this._addSupplierEntrySubject;
    }

    /**
     * @description Publishes the result of the add supplier entry process
     */
    publishAddSupplierEntryResult(supplierEntries) {
        this._addSupplierEntrySubject.next(supplierEntries);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the delete supplier entry process
     */
    get DeleteSupplierEntryResultSubject() {
        return this._deleteSupplierEntrySubject;
    }

    /**
     * @description Publishes the result of the delete supplier entry process
     */
    publishDeleteSupplierEntryResult(result) {
        this._deleteSupplierEntrySubject.next(result);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the delete all supplier entries process
     */
    get DeleteAllSupplierEntryResultSubject() {
        return this._deleteAllSupplierEntrySubject;
    }

    /**
     * @description Publishes the result of the delete all supplier entries process
     */
    publishDeleteAllSupplierEntryResult(result) {
        this._deleteAllSupplierEntrySubject.next(result);
    }

    /**
     * @description Gets a subject allowing to be notified about the result of the delete several supplier entries process
     */
    get DeleteSeveralSupplierEntryResultSubject() {
        return this._deleteSeveralSupplierEntrySubject;
    }

    /**
     * @description Publishes the result of the delete several supplier entries process
     */
    publishDeleteSeveralSupplierEntryResult(result) {
        this._deleteSeveralSupplierEntrySubject.next(result);
    }
}
