var env = process.env.NODE_ENV || 'development';
import c from '../config/config';
const config = c[env];
import SqlServerLoginRepository from './SqlServerLoginRepository';
import LatitudeLoginRepository from './LatitudeLoginRepository';
import LatitudeOrderRepository from './LatitudeOrderRepository';

/**
* @description Class responsible for creating repositories based on configuration and type
*/
class RepositoryFactory {

    /**
     * @description Makes and returns a repository based on configuration
     * @param {String} repotype Type of the repository to create
     * @returns A repository based on configuration and type
     */
    makeRepository(repoType) {
        if (config.repository.latitude) {
            if (repoType === 'login') {
                return new LatitudeLoginRepository();
            }
            else if (repoType === 'order') {
                return new LatitudeOrderRepository();
            }
        }
        else if (config.repository.sqlServer) {
            if (repoType === 'login') {
                return new SqlServerLoginRepository();
            }
        }
    }
}

//it allows to export a Singleton
export let repoFact = new RepositoryFactory();
