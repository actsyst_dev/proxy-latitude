/**
 * @description Class responsible for accessing data via SqlServer regarding operators
*/
export default class SqlServerLoginRepository{

    /**
     * Sends a request to get the person whose login and password are passed as parameters
     * @param {String} login Login of the person to get
     * @param {String} password Password of the person to get
     */
    getPerson(login, password){
        console.log(`Get person from SqlServer\r\nLogin : ${login}\r\nPassword : ${password}`); //eslint-disable-line no-console
    }
}
