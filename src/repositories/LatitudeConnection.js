import net from 'net';
import windows1252 from 'windows-1252';
var env = process.env.NODE_ENV || 'development';
import c from '../config/config';
const config = c[env];

/**
 * @description Class responsible for communicating with Latitude. Implements the Latitude protocol.
 */
export default class LatitudeConnection {

    /**
     * @description Constructor
     * @param {LatitudeSubject} latitudeSubject LatitudeSuject acting as mediator and allowing
     * the publication of the query results
     */
    constructor(latitudeSubject) {
        this._socket = null;
        this._pendingDatas = '';
        this._latitudeSubject = latitudeSubject;
    }

    /**
     * Instanciates a socket to Latitude and sends the authentication frame
     * Implements also the receiver function of the socket object
     */
    initializeConnection() {

        //instanciates a socket and sends the latitude authentication frame
        this._socket = net.connect(config.latitude.port, config.latitude.host, () => {
            //TODO : automating the determination of the latitude credentials
            const authString = String.fromCharCode(2) + 'PW###001|LAN1|LAN1' + String.fromCharCode(3);
            const encodedData = windows1252.encode(authString);
            this._socket.write(encodedData);
        });
        let that = this;
        //receiving socket callback
        this._socket.on("data", function (data) {
            if (data[data.length - 1] === 3) {
                //end of the frame, we delete the ETX character
                for (let i = 0; i < data.length - 1; i++) {
                    that._pendingDatas = that._pendingDatas + String.fromCharCode(data[i]);
                }
            }
            else {
                //we add to receiving to the pending data
                for (let i = 0; i < data.length; i++) {
                    that._pendingDatas = that._pendingDatas + String.fromCharCode(data[i]);
                }
            }
            //we check if it's the end of the transmitting
            if (that._pendingDatas.endsWith("#008#") || that._pendingDatas.endsWith("OK") || that._pendingDatas.endsWith("NOK")) {
                let datas = that.getArrayFromFrame();
                that.onComplete(datas);
            }
            //we check if the query result is empty
            else if (that._pendingDatas.endsWith("#005#")) {
                that.onComplete("");
            }
            //we check if the query caused an error
            else if (that._pendingDatas.endsWith("#099#") || that._pendingDatas.endsWith("#006#") || that._pendingDatas.endsWith("#550#") || that._pendingDatas.endsWith("#849#") || that._pendingDatas.endsWith("#756#") || that._pendingDatas.endsWith("#1300#")) {
                that.onComplete("Latitude error");
            }
        });
        //error socket callback
        this._socket.on("error", function (err) {
            console.log("Erreur de connexion!" + err.message); //eslint-disable-line no-console
            //TODO try reconnection
        });
    }

    /**
     * Sends a query to Latitude
     * @param {Number} databaseNumber Database number to query
     * @param {Number} queryNumber Query number
     * @param {String} datas Criteria separated by the character |
     */
    send(databaseNumber, queryNumber, datas) {
        const queryString = String.fromCharCode(2) + 'SE#MID#' + databaseNumber + '#' + queryNumber + '#' + datas + String.fromCharCode(3);
        const encodedData = windows1252.encode(queryString);
        this._pendingDatas = '';
        this._socket.write(encodedData);
    }

    /**
     * Gets an array containing the result of the query
     */
    getArrayFromFrame() {
        var retval = [];
        if (this._pendingDatas.endsWith("NOK")) {
            //authentication failed
            retval.push("NOK");
        }
        else if (this._pendingDatas.endsWith("OK")) {
            //Successful authentication
            retval.push("OK");
        }
        else {
            //records case
            while (this._pendingDatas.indexOf("SE#MID#000#") > -1) {
                this._pendingDatas = this._pendingDatas.replace("SE#MID#000#", "");
            }
            retval = this._pendingDatas.split(String.fromCharCode(2));
        }
        return retval;
    }

    /**
     * Callback receiving the result of the query. Publish it with the LatitudeSubject
     * @param {Array} data Result of the query
     */
    onComplete(data) {
        //publish to subscripters
        if (data[0] === 'OK') {
            //Successful authentication
            this._latitudeSubject.publishConnectionState({
                stateOn: true
            });
        }
        else if (data[0] === 'NOK') {
            //authentication failed
            this._latitudeSubject.publishConnectionState({
                stateOn: false
            });
        }
        else if (data === 'Latitude error') {
            //the query caused an error
            this._latitudeSubject.publishSendResult('Latitude error');
        }
        else {
            //records case
            let objects = [];
            for (let i = 0; i < data.length - 1; i++) {
                if (data[i] !== "" && !data[i].endsWith("SE#MID#008#")) {
                    objects.push(data[i].replace(String.fromCharCode(3), ''));
                }
            }
            //the query caused a validation error
            if (objects.length > 0 && objects[0].indexOf("$VALIDATION_ERROR$") !== -1) {
                this._latitudeSubject.publishSendResult('Latitude error : ' + objects[0]);
            }
            //the query returned records
            else {
                this._latitudeSubject.publishSendResult(objects);
            }
        }
    }
}

