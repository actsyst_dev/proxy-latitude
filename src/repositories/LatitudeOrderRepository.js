import LatitudeConnection from './LatitudeConnection';
import LatitudeSubject from './LatitudeSubject';
import OrderItem from '../models/OrderItem';
import SupplierEntry from '../models/SupplierEntry';

/**
 * @description Class responsible for accessing data via Latitude regarding Orders
*/
export default class LatitudeOrderRepository {

    /**
     * Default constructor
     */
    constructor() {
        this._latitudeSubject = new LatitudeSubject();
    }

    /**
     * Gets the LatitudeSubject acting as mediator
     * and allowing to subscribe to the publications of the LatitudeConnection
     */
    get LatitudeSubject() {
        return this._latitudeSubject;
    }

    /**
     * @description Gets the list of the order item linked to the order
     * whose number is passed as parameter
     * @param {String} numberOrder Number of the order
     */
    getOrderItems(numberOrder) {
        //TODO : get LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 802, numberOrder);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                let orderItems = [];
                //for each results, we instanciate an orderItem
                for (let i = 0; i < results.length; i++) {
                    let orderItem = new OrderItem();
                    let r = results[i];
                    orderItem.parse(r);
                    orderItems.push(orderItem);
                }
                //finally, we publish the list of the order items
                that._latitudeSubject.publishOrderResult(orderItems);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishOrderResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }

    /**
     * @description Gets the list of the supplier entries linked to the order item
     * whose id is passed as parameter
     * @param {Number} itemId Id of the order item
     */
    getSupplierEntries(itemId) {
        //TODO : get LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 803, itemId);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                let supplierEntries = [];
                //for each results, we instanciate a SupplierEntry
                for (let i = 0; i < results.length; i++) {
                    let supplierEntry = new SupplierEntry();
                    let r = results[i];
                    supplierEntry.parse(r);
                    supplierEntries.push(supplierEntry);
                }
                //we publish the list of the supplier entries
                that._latitudeSubject.publishSupplierEntryResult(supplierEntries);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishSupplierEntryResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }

    /**
     * @description Adds a supplier entry
     * @param {SupplierEntry} supplierEntry The supplier entry to add
     */
    addSupplierEntry(supplierEntry) {
        //TODO : Get LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 804, supplierEntry.ItemId + "|" +
                    supplierEntry.LotNumber + "|" +
                    supplierEntry.SerialNumber + "|" +
                    supplierEntry.PeremptionDate + "|" +
                    supplierEntry.Quantity);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                let supplierEntries = [];
                //the query returns all the supplier entries linked to the same order item
                //for each results, we instanciate a SupplierEntry
                for (let i = 0; i < results.length; i++) {
                    let supplierEntry = new SupplierEntry();
                    let r = results[i];
                    supplierEntry.parse(r);
                    supplierEntries.push(supplierEntry);
                }
                //we publish the list of the supplier entries
                that._latitudeSubject.publishAddSupplierEntryResult(supplierEntries);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishAddSupplierEntryResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }

    /**
     * @description Delete the supplier entry whose the id is passed as parameter
     * @param {Number} supplierEntryId The id of the supplier entry to delete
     */
    deleteSupplierEntry(supplierEntryId) {
        //TODO : Get the LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 805, supplierEntryId);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                let supplierEntries = [];
                //the query returns all the supplier entries linked to the same order item
                //for each results, we instanciate a SupplierEntry
                for (let i = 0; i < results.length; i++) {
                    let supplierEntry = new SupplierEntry();
                    let r = results[i];
                    supplierEntry.parse(r);
                    supplierEntries.push(supplierEntry);
                }
                //we publish the list of the supplier entries
                that._latitudeSubject.publishDeleteSupplierEntryResult(supplierEntries);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishDeleteSupplierEntryResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }

    /**
     * @description Delete all the supplier entries linked to the order item
     * whose the id is passed as parameter
     * @param {Number} itemId The id of the order item
     */
    deleteAllSupplierEntries(itemId) {
        //TODO : Gets the LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we send the query
                lc.send(1, 806, itemId);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error') //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //no Latitude error
                let supplierEntries = [];
                //the query returns all the supplier entries linked to the order item
                //for each results, we instanciate a SupplierEntry
                //normally, the list should be empty
                for (let i = 0; i < results.length; i++) {
                    let supplierEntry = new SupplierEntry();
                    let r = results[i];
                    supplierEntry.parse(r);
                    supplierEntries.push(supplierEntry);
                }
                //we publish the list of the supplier entries
                that._latitudeSubject.publishDeleteAllSupplierEntryResult(supplierEntries);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishDeleteAllSupplierEntryResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }

    /**
     * @description Delete the supplier entries whose the id is contained
     * in the array passed as parameter
     * @param {Array} supplierEntriesId Array containing the id of the supplier entries to delete
     */
    deleteSeveralSupplierEntries(supplierEntriesId) {
        //TODO : Gets the LatitudeConnection from the pooler
        let that = this;
        //we subscribe to a connection state subject to be notified of the result
        //of the Latitude connexion attempt
        this._latitudeSubject.ConnectionStateSubject.subscribe((result) => {
            if (result.stateOn) {
                //Successful connection, we join the elements of the array and send the query
                let parameters = supplierEntriesId.join();
                lc.send(1, 807, parameters);
            }
            else {
                //Connection or authentication failed
                //TODO : management of the error
                console.log('Error'); //eslint-disable-line no-console
            }
        }
        );

        //we subscribe to a send result subject to be notified of the query result
        this._latitudeSubject.SendResultSubject.subscribe((results) => {
            if (results.indexOf('Latitude error') === -1) {
                //No Latitude error
                let supplierEntries = [];
                //the query returns all the supplier entries linked to the same order item
                //for each results, we instanciate a SupplierEntry
                for (let i = 0; i < results.length; i++) {
                    let supplierEntry = new SupplierEntry();
                    let r = results[i];
                    supplierEntry.parse(r);
                    supplierEntries.push(supplierEntry);
                }
                //we publish the list of the supplier entries
                that._latitudeSubject.publishDeleteSeveralSupplierEntryResult(supplierEntries);
            }
            else {
                //we publish the error
                that._latitudeSubject.publishDeleteSeveralSupplierEntryResult(results);
            }
        });
        //we instanciate a LatitudeConnection
        //and pass it the LatitudeSubject wich allows the pub/sub communication
        let lc = new LatitudeConnection(this._latitudeSubject);
        //we ask the socket connection and the sending of the Latitude authentication frame
        lc.initializeConnection();
    }
}

