import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
//import passport from 'passport';
import jwt from 'express-jwt';
var env = process.env.NODE_ENV || 'development';
import c from './config/config';
const config = c[env];
import MyPassport from './config/MyPassport';
import LoginRoute from './routes/LoginRoute';
import OrderRoute from './routes/OrderRoute';

//create express application
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// jwt check token
app.use(jwt({
    secret: config.auth.secret,
    getToken: (req) => {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === "Bearer") {
            return req.headers.authorization.split(' ')[1];
        }
        else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({
    //Array where the exceptions must be inserted
    path: [
        "/api/login",
        '/',
        "/index.html"
    ]
}));

let myPassport = new MyPassport();
myPassport.usePassport(app);

//route settings
app.use("/api", new LoginRoute().Router);
app.use("/api", new OrderRoute().Router);

//default get from root
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
})

//start listening
const port = process.env.PORT || config.express.port;
app.listen(port, () => {
    console.log(`API running on localhost:${port}`); //eslint-disable-line no-console
});
