import express from 'express';
import passport from 'passport';

export default class LoginRoute{

    constructor(){
        this._router = express.Router();
        this.initialize();
    }

    get Router(){
        return this._router;
    }

    initialize(){
        this._router.post('/login', (req, res, next) => { //eslint-disable-line no-unused-vars
            //console.log(req.params);
            //console.log(req.body);

            passport.authenticate("local", (err, person, info) => {
                if (err) {
                    res.status(404).json(err);
                    return;
                }
                if (person) {
                    //credentials checking
                    var token = person.generateToken();
                    res.status(200).json({
                        "token":token,
                        "person":person
                    })
                }
                else {
                    res.status(401).json(info);
                }
            })(req, res);
        });
    }
}
