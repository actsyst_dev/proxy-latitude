import express from 'express';
import {repoFact} from '../repositories/RepositoryFactory';
import SupplierEntry from '../models/SupplierEntry';

export default class OrderRoute{

    constructor(){
        this._router = express.Router();
        this.initialize();
    }

    get Router(){
        return this._router;
    }

    initialize(){
        this._router.get('/orderItems/:number', (req, res, next) => { //eslint-disable-line no-unused-vars

            let number = req.params.number;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.OrderResultSubject
                .subscribe((orderItems) => {
                    if(typeof orderItems === "string" && orderItems.indexOf('Latitude error')!==-1){
                        res.status(500).json(orderItems);
                    }
                    else{
                        res.status(200).json(orderItems);
                    }
                });

            repo.getOrderItems(number);
        });

        this._router.get('/supplierEntries/:itemId', (req, res, next) => { //eslint-disable-line no-unused-vars

            let itemId = req.params.itemId;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.SupplierEntryResultSubject
                .subscribe((supplierEntry) => {
                    if(typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error')!==-1){
                        res.status(500).json(supplierEntry);
                    }
                    else{
                        res.status(200).json(supplierEntry);
                    }
                });

            repo.getSupplierEntries(itemId);
        });

        this._router.post('/supplierEntries/add', (req, res, next) => { //eslint-disable-line no-unused-vars

            let supplierEntry = new SupplierEntry();
            supplierEntry.ItemId = req.body.itemId;
            supplierEntry.LotNumber = req.body.lotNumber;
            supplierEntry.SerialNumber = req.body.serialNumber;
            supplierEntry.PeremptionDate = req.body.peremptionDate;
            supplierEntry.Quantity = req.body.quantity;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.AddSupplierEntryResultSubject
                .subscribe((supplierEntry) => {
                    if(typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error')!==-1){
                        res.status(500).json(supplierEntry);
                    }
                    else{
                        res.status(200).json(supplierEntry);
                    }
                });

            repo.addSupplierEntry(supplierEntry);
        });

        this._router.post('/supplierEntries/delete/:supplierEntryId', (req, res, next) => { //eslint-disable-line no-unused-vars

            let supplierEntryId = req.params.supplierEntryId;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.DeleteSupplierEntryResultSubject
                .subscribe((supplierEntry) => {
                    if(typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error')!==-1){
                        res.status(500).json(supplierEntry);
                    }
                    else{
                        res.status(200).json(supplierEntry);
                    }
                });

            repo.deleteSupplierEntry(supplierEntryId);
        });

        this._router.post('/supplierEntries/deleteSeveral', (req, res, next) => { //eslint-disable-line no-unused-vars

            let supplierEntriesId = req.body;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.DeleteSeveralSupplierEntryResultSubject
                .subscribe((supplierEntry) => {
                    if(typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error')!==-1){
                        res.status(500).json(supplierEntry);
                    }
                    else{
                        res.status(200).json(supplierEntry);
                    }
                });

            repo.deleteSeveralSupplierEntries(supplierEntriesId);
        });

        this._router.post('/supplierEntries/deleteAll/:itemId', (req, res, next) => { //eslint-disable-line no-unused-vars

            let itemId = req.params.itemId;

            let repo = repoFact.makeRepository('order');
            repo.LatitudeSubject.DeleteAllSupplierEntryResultSubject
                .subscribe((supplierEntry) => {
                    if(typeof supplierEntry === "string" && supplierEntry.indexOf('Latitude error')!==-1){
                        res.status(500).json(supplierEntry);
                    }
                    else{
                        res.status(200).json(supplierEntry);
                    }
                });

            repo.deleteAllSupplierEntries(itemId);
        });
    }
}
