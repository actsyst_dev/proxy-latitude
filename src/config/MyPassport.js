import passport from "passport";
import LocalStrategy from './strategies/LocalStrategy';

/**
 * @description Class responsible for initializing and setting the passport with a local strategy
 */
export default class MyPassport {

    /**
     * @description Set the passport with a local strategy, initialize it,
     * and mount it to the express application passed in parameter
     * @param {Express} app Express application that will use the passport
     */
    usePassport(app) {
        //we instanciate a LocalStrategy object and apply the builtin strategy
        let ls = new LocalStrategy();
        ls.applyStrategy();
        //we mount the passport object initialized to the express application
        app.use(passport.initialize());
    }
}
