import passport from 'passport';
import { repoFact } from '../../repositories/RepositoryFactory';
import ls from 'passport-local';

/**
 * @description Class responsible for applying a passport local strategy for authenticating
 * with login and password
*/
export default class LocalStrategy {

    /**
     * @description Apply the passport local strategy
     */
    applyStrategy() {
        const LocStra = ls.Strategy;

        //set the passport to use a local strategy
        passport.use(new LocStra(
            {
                //we'll use login and password fields
                usernameField: 'login',
                passwordField: 'password'
            },
            (login, password, done) => {
                //ask to the factory witch repository to use
                let repo = repoFact.makeRepository('login');
                //the repository provides a pub/sub mecanism to be notified of the response
                //a LatitudeSubject object managing this pub/sub mecanism is exposed by the repository
                //we use the LatitudeSubject by subscribing to the LoginResultSubject that it provides
                repo.LatitudeSubject.LoginResultSubject
                    .subscribe((person) => {
                        //we get the response
                        return done(null, person);
                    });
                //we ask to the repository to get the person via is own data access mecanism
                repo.getPerson(login, password);
            }
        ))
    }
}
