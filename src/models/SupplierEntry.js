/**
 * @description Business class implementing an supplier entry
 */
export default class SupplierEntry {

    /**
     * Default constructor
     */
    constructor() {
        this._supplierEntryId = new Number(0);
        this._itemId = new Number(0);
        this._lotNumber = '';
        this._serialNumber = '';
        this._peremptionDate = '';
        this._quantity = new Number(0);
    }

    /**
     * @description Gets the id of the supplier entry
     */
    get SupplierEntryId() {
        return this._supplierEntryId;
    }

    /**
     * @description Sets the id of the supplier entry
     * @param {Number} supplierEntryId Id to set
     */
    set SupplierEntryId(supplierEntryId) {
        this._supplierEntryId = supplierEntryId;
    }

    /**
     * @description Gets the id of the order item linked to this supplier entry
     */
    get ItemId() {
        return this._itemId;
    }

    /**
     * @description Sets the id of the order item linked to this supplier entry
     * @param {Number} itemId Id of the order item to set
     */
    set ItemId(itemId) {
        this._itemId = itemId;
    }

    /**
     * @description Gets the lot number of the supplier entry
     */
    get LotNumber() {
        return this._lotNumber;
    }

    /**
     * @description Sets the lot number of the supplier entry
     * @param {String} lotNumber Lot number of the supplier entry
     */
    set LotNumber(lotNumber) {
        this._lotNumber = lotNumber;
    }

    /**
     * @description Gets the serial number of the supplier entry
     */
    get SerialNumber() {
        return this._serialNumber;
    }

    /**
     * @description Sets the serial number of the supplier entry
     * @param {String} serialNumber Serial number of the supplier entry
     */
    set SerialNumber(serialNumber) {
        this._serialNumber = serialNumber;
    }

    /**
     * @description Gets the peremption date of the supplier entry
     */
    get PeremptionDate() {
        return this._peremptionDate;
    }

    /**
     * @description Sets the peremption date of the supplier entry
     * @param {String} peremptionDate Peremption date of the supplier entry
     */
    set PeremptionDate(peremptionDate) {
        this._peremptionDate = peremptionDate;
    }

    /**
     * @description Gets the quantity of the supplier entry
     */
    get Quantity() {
        return this._quantity;
    }

    /**
     * @description Sets the quantity of the supplier entry
     * @param {Number} quantity Quantity of the supplier entry
     */
    set Quantity(quantity) {
        this._quantity = quantity;
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */
    parse(data) {
        let datas = data.split('|');
        this._supplierEntryId = new Number(datas[0]);
        this._lotNumber = datas[1];
        this._serialNumber = datas[2];
        this._peremptionDate = datas[3];
        this._quantity = new Number(datas[4]);
        this._itemId = new Number(datas[5]);
    }
}
