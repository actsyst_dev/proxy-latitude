import Supplier from './Supplier';

/**
 * @description Business class implementing an order
 */
export default class Order {

    /**
     * Default constructor
     */
    constructor() {
        this._orderItems = [];
        this._supplier;
        this._orderNumber;
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */
    parse(data) {
        let datas = data.split('|');
        this._orderNumber = datas[6];
        this._supplier = new Supplier();
        this._supplier.Code = datas[7];
        this._supplier.Name = datas[8];
    }
}
