import jwt from 'jsonwebtoken';
var env = process.env.NODE_ENV || 'development';
import c from '../config/config';
const config = c[env];

/**
 * @description Business class implementing an person
 */
export default class Person {

    /**
     * Default constructor
     */
    constructor() {
        this.id = -1;
        this.login = '';
        this.password = '';
        this.firstName = '';
        this.lastName = '';
        this.qualification = '';
        this.token = '';
        this._orders = [];
    }

    /**
     * Generates a security token by using jwt library and inner properties
     */
    generateToken() {
        var expiry = new Date();
        expiry.setDate(expiry.getDate() + 7);
        this.token = jwt.sign({
            email: this.login,
            exp: parseInt(expiry.getTime() / 1000)
        }, config.auth.secret);
        return this.token;
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */
    parse(data) {
        let datas = data.split('|');
        this.id = new Number(datas[0]);
        this.firstName = datas[1];
        this.lastName = datas[2];
        this.login = datas[4];
        this.password = datas[5];
    }

    /**
     * Adds an order to the orders array of the person
     * @param {Order} order Order to add
     */
    addOrder(order) {
        this._orders.push(order);
    }
}
