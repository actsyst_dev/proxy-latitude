/**
 * @description Business class implementing an article
 */
export default class Article {

    /**
     * @description Default constructor
     */
    constructor() {
        this._ref = '';
        this._subRef1 = '';
        this._subRef2 = '';
        this._designation = '';
        this._isLotNumber = false;
        this._isSerialNumber = false;
        this._isPeremptionDate = false;
    }

    /**
     * @description Gets the reference of the article
     */
    get Ref() {
        return this._ref;
    }

    /**
     * @description Sets the reference of the article
     * @param {String} ref Reference to set
     */
    set Ref(ref) {
        this._ref = ref;
    }

    /**
     * @description Gets the first sub reference of the article
     */
    get SubRef1() {
        return this._subRef1;
    }

    /**
     * @description Sets the first sub reference of the article
     * @param {String} subRef1 First sub reference to set
     */
    set SubRef1(subRef1) {
        this._subRef1 = subRef1;
    }

    /**
     * @description Gets the second sub reference of the article
     */
    get SubRef2() {
        return this._subRef2;
    }

    /**
     * @description Sets the second sub reference of the article
     * @param {String} subRef2 Second sub reference to set
     */
    set SubRef2(subRef2) {
        this._subRef2 = subRef2;
    }

    /**
     * @description Gets the designation of the article
     */
    get Designation() {
        return this._designation;
    }

    /**
     * @description Sets the designation of the article
     * @param {String} designation Designation to set
     */
    set Designation(designation) {
        this._designation = designation;
    }

    /**
     * @description Gets a boolean that defines whether the article requires a lot number
     */
    get IsLotNumber() {
        return this._isLotNumber;
    }

    /**
     * @description Sets a boolean that defines whether the article requires a lot number
     * @param {Boolean} isLotNumber True whether the article requires a lot number
     */
    set IsLotNumber(isLotNumber) {
        this._isLotNumber = isLotNumber;
    }

    /**
     * @description Gets a boolean that defines whether the article requires a serial number
     */
    get IsSerialNumber() {
        return this._isSerialNumber;
    }

    /**
     * @description Sets a boolean that defines whether the article requires a serial number
     * @param {Boolean} isSerialNumber True whether the article requires a serial number
     */
    set IsSerialNumber(isSerialNumber) {
        this._isSerialNumber = isSerialNumber;
    }

    /**
     * @description Gets a boolean that defines whether the article requires a peremption date
     */
    get IsPeremptionDate() {
        return this._isPeremptionDate;
    }

    /**
     * @description Sets a boolean that defines whether the article requires a peremption date
     * @param {Boolean} isPeremptionDate True whether the article requires a peremption date
     */
    set IsPeremptionDate(isPeremptionDate) {
        this._isPeremptionDate = isPeremptionDate;
    }
}
