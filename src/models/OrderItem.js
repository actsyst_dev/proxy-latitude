import Article from './Article';

/**
 * @description Business class implementing an order item
 */
export default class OrderItem {

    /**
     * Default constructor
     */
    constructor() {
        this._itemId = new Number(0);
        this._article = new Article();
        this._orderedQuantity = new Number(0);
        this._printedQuantity = new Number(0);
    }

    /**
     * @description Splits a string by applying the Latitude protocol
     *  and sets the properties of the object
     * @param {String} data Data string from Latitude to split
     */
    parse(data) {
        let datas = data.split('|');
        this._itemId = new Number(datas[0]);
        this._article.Ref = datas[1];
        this._article.Designation = datas[2];
        this._article.IsLotNumber = new Boolean(datas[3] === '1');
        this._article.IsSerialNumber = new Boolean(datas[4] === '1');
        this._article.IsPeremptionDate = new Boolean(datas[5] === '1');
        this._orderedQuantity = new Number(datas[6]);
        this._printedQuantity = new Number(datas[7]);
        this._article.SubRef1 = datas[8];
        this._article.SubRef2 = datas[9];
    }
}
