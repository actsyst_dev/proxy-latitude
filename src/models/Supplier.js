/**
 * @description Business class implementing an supplier
 */
export default class Supplier {

    /**
     * Default constructor
     */
    constructor() {
        this._code = '';
        this._name = '';
        this._country = '';
    }

    /**
     * @description Sets the code of the supplier
     * @param {String} code Code to set
     */
    set Code(code) {
        this._code = code;
    }

    /**
     * @description Sets the name of the supplier
     * @param {String} name Name to set
     */
    set Name(name) {
        this._name = name;
    }

    /**
     * @description Sets the country of the supplier
     * @param {String} country Country to set
     */
    set Country(country) {
        this._country = country;
    }
}
