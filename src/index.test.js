import { expect } from 'chai';
import jsdom from 'jsdom';

describe('Our first test', () => {
    it('should pass', () => {
        expect(true).to.equal(true);
    });
});

describe('index.html', () => {
    it('should have h2 that says hello proxy lattitude', () => {
        const { JSDOM } = jsdom;
        return new Promise((resolve, reject) => {
            JSDOM.fromFile("./src/index.html", null).then(dom => {
                const h2 = dom.window.document.getElementsByTagName('h2')[0];
                expect(h2.innerHTML).to.equal("hello proxy lattitude");
                dom.window.close();
                return resolve();
            })
                .catch((error) => {
                    return reject(error);
                });
        });
    }
    )
});
